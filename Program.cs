﻿using System;
using System.ServiceProcess;


namespace inin.Bridge.WebServices.Datadip.SqlConnector
{
    class Program
    {
        static void Main(string[] args)
        {
            //#if DEBUG
            if (Environment.UserInteractive)
            {
                // Debug code: this allows the process to run as a non-service.
                // It will kick off the service start point, but never kill it.
                // Shut down the debugger to exit
                var progsvc = new BridgeSqlConnectorWindowsService();
                progsvc.Start();

                Console.ReadLine();
                return;
            }
            //#endif
            var servicesToRun = new ServiceBase[] 
            { 
                new BridgeSqlConnectorWindowsService() 
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}

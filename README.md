# Bridge SQL Connector #

This Bridge connector for PureCloud allows to query MS SQL database and returns output row as a comma delimited string to Architect. The query should return just one output row.

### How do I get set up? ###

* Download the latest version from "Download" section
* Install the BridgeSqlConnector.msi on the Bridge server
* Go to C:\Program Files (x86)\Interactive Intelligence\BridgeSQLConnector folder 
and inside configuration file: 

```
#!c#

inin.Bridge.WebServices.Datadip.SqlConnector.exe.config 
```

put your valid connection string, e.g.: 

```
#!c#

<add key="ConnectionString" value="Data Source=127.0.0.1;Initial Catalog=I3_IC;User ID=sa;Password=1234;Pooling=true" />
```

* Start "ININ BridgeSqlConnector" windows service

#### PureCloud configuration ####
* Add a Web Services Data Dip Connector as described here: https://help.mypurecloud.com/articles/add-web-services-data-dip-connector/ and enter a name, e.g.: WebServicesSqlConnector, choose the connector: webservices-datadip and click Save
* Configure the connector as described here: https://help.mypurecloud.com/articles/configure-web-services-data-dip-connector/ and put PluginName: webservices-sqlconnector and EndpointAddress: http://localhost:8889/
* Add bridge action
as described here: https://help.mypurecloud.com/articles/add-bridge-actions-web-services-data-dip-connector/ Select 'WebServiceSqlConnector' group and 'GetAccountByAccountNumber' action and click next button. Name the action as 'WSSQL_GetAccountByAccountNo' and add category name 'WSSQL'. Check 'Flatten metadata' and click add action. Publish the action.

### How do I use it? ###
Please check this instructions first: https://help.mypurecloud.com/articles/use-bridge-actions-architect-web-services-data-dip-connector/

Add a new bridge action in Architect. Select category as 'WSSQL' and select action 'WSSQL_GetAccountByAccountNo'.

Enter your query as Input 'CustomAttribute', e.g.: Append("SELECT * FROM [Telbook].[dbo].[employees] where RowNo=",Flow.sAccountNumber)

AccountNumber should be an empty string, e.g.: ""

Query result will be returned as Account.CustomAttribute
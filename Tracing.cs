﻿using System;
using System.Diagnostics;

namespace inin.Bridge.WebServices.Datadip.SqlConnector
{
    public static class Tracing
    {
        private const string Source = "BridgeSqlConnectorService";

        public static void status(string message)
        {
            EventLog.WriteEntry(Source, message, EventLogEntryType.Information);
        }

        public static void exception(Exception ex, string message)
        {
            EventLog.WriteEntry(Source, string.Format("{0}: {1}", message, ex.Message), EventLogEntryType.Error);
        }
    }
}

﻿using System;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess;

namespace inin.Bridge.WebServices.Datadip.SqlConnector
{
    public partial class BridgeSqlConnectorWindowsService : ServiceBase
    {
        private SqlWebServices sqlWebService;

        public BridgeSqlConnectorWindowsService()
        {
            InitializeComponent();
        }

        public void Start()
        {
            this.OnStart(new string[] { });
        }

        protected override void OnStart(string[] args)
        {
#if DEBUG
            System.Diagnostics.Debugger.Launch();
#endif
            try
            {
                Type t = this.GetType();
                Assembly assembly = t.Assembly;
                AssemblyName assamblyName = assembly.GetName();
                string port = ConfigurationManager.AppSettings["Port"];
                Tracing.status(string.Format("Service starting (version: {0}, address: http://127.0.0.1:{1})", assamblyName.Version.ToString(), port));
                this.sqlWebService = new SqlWebServices();
                this.sqlWebService.Start();

                base.OnStart(args);
                Tracing.status("Service started");
            }
            catch (Exception ex)
            {
                Tracing.exception(ex, "Unable to start service");
                this.StopInternal();
                throw;
            }
        }

        protected override void OnStop()
        {
            try
            {
                Tracing.status("Service stopping");
                this.StopInternal();
                base.OnStop();
                Tracing.status("Service stopped");
            }
            catch (Exception ex)
            {
                Tracing.exception(ex, "Error stopping service");
            }
        }

        private void StopInternal()
        {
            if (this.sqlWebService != null)
                this.sqlWebService.Stop();
        }

    }
}

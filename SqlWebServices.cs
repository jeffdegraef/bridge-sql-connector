﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using inin.Bridge.WebServices.Datadip.Lib;

namespace inin.Bridge.WebServices.Datadip.SqlConnector
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SqlWebServices : IWebServicesServer
    {
        private const string Separator = ";";
        private readonly WebServiceHost serviceHost;

        public SqlWebServices()
        {
            string port = ConfigurationManager.AppSettings["Port"];
            serviceHost = new WebServiceHost(this, new Uri("http://127.0.0.1:" + port));
        }

        public void Start()
        {
            this.serviceHost.Open();
        }

        public void Stop()
        {
            this.serviceHost.Abort();
        }

        public ResponseAccount GetAccountByAccountNumber(AccountNumberRequest req)
        {
            string query = null;
            try
            {
                string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                query = req.CustomAttribute.Replace("&apos;", "'");

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    using (var sqlCmd = new SqlCommand(query, sqlConnection))
                    {
                        sqlConnection.Open();
                        using (var reader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                ResponseAccount retVal = new ResponseAccount();
                                Account retAccount = new Account();

                                var sb = new StringBuilder();
                                for (int index = 0; index < reader.FieldCount - 1; index++)
                                {
                                    if (!reader.IsDBNull(index))
                                    {
                                        string value = reader.GetValue(index).ToString();
                                        if (reader.GetFieldType(index) == typeof (String))
                                        {
                                            //If double quotes are used in value, ensure each are replaced but 2.
                                            if (value.IndexOf("\"", StringComparison.OrdinalIgnoreCase) >= 0)
                                                value = value.Replace("\"", "\"\"");

                                            //If separtor are is in value, ensure it is put in double quotes.
                                            if (value.IndexOf(Separator, StringComparison.OrdinalIgnoreCase) >= 0)
                                                value = "\"" + value + "\"";
                                        }
                                        sb.Append(value);
                                    }

                                    if (index < reader.FieldCount - 1)
                                        sb.Append(Separator);
                                }

                                if (!reader.IsDBNull(reader.FieldCount - 1))
                                    sb.Append(reader.GetValue(reader.FieldCount - 1).ToString().Replace(Separator, " "));

                                retAccount.CustomAttribute = sb.ToString();
                                retVal.Account = retAccount;
                                return retVal;
                            }
                            else
                                throw new WebFaultException<string>("No results found", HttpStatusCode.NoContent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.exception(ex, "Query: " + query);
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResponseAccount GetAccountByContactId(ContactIdRequest cidr)
        {
            throw new NotImplementedException();
        }

        public ResponseAccount GetAccountByPhoneNumber(PhoneNumberRequest req)
        {
            throw new NotImplementedException();
        }

        public ResponseContact GetContactByPhoneNumber(PhoneNumberRequest req)
        {
            throw new NotImplementedException();
        }

        public ResponseCase GetMostRecentOpenCaseByContactId(ContactIdRequest cidr)
        {
            throw new NotImplementedException();
        }
    }
}
